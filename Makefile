# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/31 11:27:04 by mmeisson          #+#    #+#              #
#    Updated: 2019/03/08 11:54:11 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

SRC_PATH = ./srcs/
SRC_NAME = ft_strlen.c ft_strcpy.c ft_strncpy.c ft_strdup.c ft_strcat.c\
	ft_strncat.c ft_strlcat.c\
	ft_strchr.c ft_strrchr.c ft_strstr.c ft_strnstr.c ft_strcmp.c ft_strncmp.c\
	ft_strtolower.c ft_strtoupper.c ft_strchri.c\
	\
	ft_strnew.c ft_strndup.c ft_strdel.c ft_strclr.c ft_striter.c ft_striteri.c\
	ft_strmap.c\
	ft_strmapi.c ft_strequ.c ft_strnequ.c ft_strsub.c ft_strjoin.c ft_strover.c\
	ft_strtrim.c\
	ft_strsplit.c ft_whitesplit.c ft_strnjoin.c ft_strnover.c ft_strrealloc.c\
	ft_strreplace.c\
	\
	ft_memalloc.c ft_memdel.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c\
	ft_memchr.c\
	ft_memcmp.c ft_memset.c ft_memdup.c\
	\
	ft_atoi.c ft_atod.c ft_itoa.c ft_itoa_base.c ft_min.c ft_max.c ft_umin.c ft_umax.c\
	\
	ft_isalpha.c ft_isdigit.c ft_isalnum.c ft_isascii.c ft_isprint.c\
	ft_tolower.c ft_toupper.c ft_isnumber.c ft_isspace.c \
	\
	ft_putchar.c ft_putstr.c ft_putendl.c ft_putnbr.c ft_putchar_fd.c\
	ft_putstr_fd.c\
	ft_putendl_fd.c ft_putnbr_fd.c ft_putnstr.c ft_putnendl.c ft_putnstr_fd.c\
	ft_putnendl_fd.c\
	\
	ft_lstnew.c ft_lstdelone.c ft_lstdel.c ft_lstadd.c ft_lstiter.c ft_lstmap.c\
	ft_lstback.c ft_lstnew_cpy.c ft_lstlen.c ft_lstin.c\
	ft_lstsort.c ft_lstjoin.c\
	\
	ft_btreenew.c ft_btreeadd.c ft_btreedelone.c ft_btreedel.c ft_btreersrc.c\
	ft_btreeiter.c\
	ft_btreemap.c ft_btreelevelcount.c ft_btreeequ.c\
	\
	ft_sortinsert.c ft_swap.c merge_list.c merge_list_r.c\
	\
	ft_tablen.c ft_tabappend.c ft_tabnappend.c ft_tabdel.c ft_tabjoin.c ft_tabiter.c \
	\
	get_next_line.c

OBJ_PATH = ./.objs/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./incs/

CFLAGS = -Wall -Werror -Wextra -Ofast -Wno-unused-result -MD

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
INC = $(addprefix -I,$(INC_PATH))

DEPS = $(OBJ:.o=.d)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	@rm -rf $(OBJ_PATH)

fclean: clean
	@rm -f $(NAME)

re: fclean all

-include $(DEPS)
