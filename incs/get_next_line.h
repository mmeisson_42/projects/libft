/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 11:43:45 by mmeisson          #+#    #+#             */
/*   Updated: 2019/03/08 11:45:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE		64

# if BUFF_SIZE < 1
#  error
# endif

int				get_next_line(int fd, char **line);

typedef struct	s_file
{
	char	*remain;
	int		fd;
}				t_file;

#endif
